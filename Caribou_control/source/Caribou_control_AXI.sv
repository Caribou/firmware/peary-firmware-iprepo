//                              -*- Mode: Verilog -*-
// Filename        : Caribou_control_AXI.sv
// Description     : AXI state machines.
// Author          : Adrian Fiergolski
// Created On      : Tue May  9 16:19:40 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module Caribou_control_AXI  #(AXI_DATA_BYTE_WIDTH = 4)
  (
   AXI4LiteInterface.slave axi
   );

   //The new register should be added here
   //Except FIRMWARE_VERSION they will be implemented as R/W registers
   enum logic [$bits(axi.awaddr)-1 : 0] {FIRMWARE_VERSION ='h00} REGISTER_ADDRESS;

   struct {
      logic en;
      logic [$bits(axi.wdata) -1 : 0] d;
      logic [$bits(axi.rdata) -1 : 0] q;} registers[ REGISTER_ADDRESS.last()/AXI_DATA_BYTE_WIDTH + 1];

   logic [31:0] 		      firmware_ver;
   
   USR_ACCESSE2 firmware_version (.CFGCLK(),
				  .DATA(firmware_ver),
				  .DATAVALID()
				  );
   assign registers[FIRMWARE_VERSION / AXI_DATA_BYTE_WIDTH].q = firmware_ver;

   wire [$bits(axi.awaddr)-$clog2(AXI_DATA_BYTE_WIDTH) -1 : 0] awaddr;
   logic 						       awaddr_supported;
   assign awaddr = axi.awaddr[$bits(axi.awaddr)-1 : $clog2(AXI_DATA_BYTE_WIDTH)];
   always_comb begin
      awaddr_supported = 0;
      if (axi.awaddr[$clog2(AXI_DATA_BYTE_WIDTH)-1 : 0] == 0 ) //all aligned transfers are supported
	awaddr_supported = 1;
      else begin
	 //The information on the low-order address lines must be consistent
	 //with the information contained on the byte lane strobes.
	 awaddr_supported = 1;
	 foreach(axi.wstrb[i])
	   if(i<axi.awaddr[$clog2(AXI_DATA_BYTE_WIDTH)-1 : 0])
	     awaddr_supported &= (axi.wstrb[i] == 0);
	   else
	     awaddr_supported &= (axi.wstrb[i] == 1);
      end
   end

   wire [$bits(axi.awaddr)-$clog2(AXI_DATA_BYTE_WIDTH) -1 : 0] araddr;
   logic 						       araddr_supported;
   assign araddr = axi.araddr[$bits(axi.araddr)-1 : $clog2(AXI_DATA_BYTE_WIDTH)];
   //The block doesn't support unaligned access
   assign araddr_supported = axi.araddr[$clog2(AXI_DATA_BYTE_WIDTH)-1 : 0] == 0 ? 1 : 0;

   //////////////
   //Write FSM
   ////////////////
   typedef enum {IDLE_W, WRITE_OK, WRITE_ERR} STATE_WRITE_T;
   STATE_WRITE_T stateW, stateW_n;

   always_comb begin : WRITE_COMB_FSM
      //Defaults
      axi.awready = 1'b0;
      axi.wready = 1'b0;
      axi.bresp = axi.OKAY;
      axi.bvalid = 1'b0;

      foreach(registers[i]) begin
	 registers[i].en = 1'b0;
	 registers[i].d = 'x;
      end

      stateW_n = stateW;      

      case (stateW)
	IDLE_W : begin
	   if(axi.wvalid) begin        //valid data
	      if( axi.awvalid ) begin  //valid address
		 if( (awaddr <= REGISTER_ADDRESS.last() / AXI_DATA_BYTE_WIDTH ) &&
		     awaddr_supported &&
		     (awaddr != FIRMWARE_VERSION[$bits(axi.awaddr)-1 : $clog2(AXI_DATA_BYTE_WIDTH)] ) //read only register
		     ) begin
		    foreach(registers[addr])
		      if(addr==awaddr) begin
			 registers[awaddr].en = 1'b1;
			
			 foreach(axi.wstrb[i]) //apply wstrb
			   registers[awaddr].d[i*8 +:8] = axi.wstrb[i] ? axi.wdata[i*8 +: 8] : registers[awaddr].q[i*8 +:8];
		      end

		    axi.awready = 1'b1;
		    axi.wready = 1'b1;
		    axi.bvalid = 1'b1;

		    if( ~axi.bready)
		      stateW_n = WRITE_OK;
		 end
		 else begin
		    axi.awready = 1'b1;
		    axi.wready = 1'b1;

		    axi.bvalid = 1'b1;
		    axi.bresp = axi.DECERR;

		    if( ~axi.bready)
		      stateW_n = WRITE_ERR;
		 end // else: !if( awaddr < REGISTER_ADDRESS.last() )
	      end // if ( axi.awvalid )
	   end // if (axi.wvalid)
	end // case: IDLE

	WRITE_OK : begin
	   axi.bvalid = 1'b1;
	   if(axi.bready)
	     stateW_n = IDLE_W;
	end

	WRITE_ERR : begin
	   axi.bresp = axi.DECERR;
	   axi.bvalid = 1'b1;
	   
	   if(axi.bready)
	     stateW_n = IDLE_W;
	end
      endcase // case (state)
   end // block: WRITE_COMB_FSM

   always_ff @(posedge axi.aclk, negedge axi.aresetN) begin : WRITE_SEQ_FSM
      if ( ~ axi.aresetN ) begin
	 stateW <= IDLE_W;
	 foreach(registers[i]) begin
	    //skip for special firmware version register
	    if( i == FIRMWARE_VERSION[$bits(axi.awaddr)-1 : $clog2(AXI_DATA_BYTE_WIDTH)] )
	      continue;
	    
	    registers[i].q <= '0;
	 end
      end
      else begin
	 stateW <= stateW_n;

	 foreach(registers[i]) begin
	    //exclude read only registers
	    if( i == FIRMWARE_VERSION[$bits(axi.awaddr)-1 : $clog2(AXI_DATA_BYTE_WIDTH)] )
	      continue;
	    if(registers[i].en) //store the output register
	      registers[i].q <= registers[i].d;
	 end
      end
   end // block: WRITE_SEQ_FSM

   //////////////
   //READ FSM
   /////////////
   typedef enum {IDLE_R, READ_OK, READ_ERR} STATE_READ_T;
   STATE_READ_T stateR, stateR_n;

   always_comb begin : READ_COMB_FSM
      //Defaults
      axi.arready = 1'b1; //accept address immediately
      axi.rvalid = 1'b0;
      axi.rresp = axi.OKAY;
      axi.rvalid = 1'b0;
      axi.rdata = 'x;

      stateR_n = stateR;      

      case (stateR)
	IDLE_R : begin
	   if( axi.arvalid ) begin  //valid address
	      if( (araddr <= REGISTER_ADDRESS.last() / AXI_DATA_BYTE_WIDTH) &&
		  araddr_supported ) begin
		 axi.rdata = registers[ araddr ].q;

		 axi.rvalid = 1'b1;

		 if(~axi.rready)
		   stateR_n = READ_OK;
	      end
	      else begin
		 axi.rvalid = 1'b1;

		 axi.rresp = axi.DECERR;

		 if(~axi.rready)
		   stateR_n = READ_ERR;
	      end // else: !if( araddr < REGISTER_ADDRESS.num() )
	   end // if ( axi.arvalid )
	end // case: IDLE

	READ_OK : begin
	   axi.rvalid = 1'b1;
	   axi.rdata = registers[ araddr ].q;
	   
	   if(axi.rready)
	     stateR_n = IDLE_R;
	end

	READ_ERR : begin
	   axi.rvalid = 1'b1;
	   axi.rresp = axi.DECERR;
	   
	   if(axi.rready)
	     stateR_n = IDLE_R;
	end
      endcase // case (state)
   end // block: READ_COMB_FSM

   always_ff @(posedge axi.aclk, negedge axi.aresetN) begin : READ_SEQ_FSM
      if ( ~ axi.aresetN ) begin
	 stateR <= IDLE_R;
      end
      else begin
	 stateR <= stateR_n;
      end
   end // block: READ_SEQ_FSM

endmodule // Caribou_control_AXI
