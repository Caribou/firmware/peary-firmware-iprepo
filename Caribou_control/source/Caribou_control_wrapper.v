//                              -*- Mode: Verilog -*-
// Filename        : Caribou_control_wrapper.v
// Description     : Wrapper of the Caribou control.
// Author          : Adrian Fiergolski
// Created On      : Tue May  9 16:35:12 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module Caribou_control_wrapper  #(AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 8)
   (
    ///////////////////////
    //AXI4-Lite interface
    ///////////////////////

    //Write address channel
	input wire [AXI_ADDR_WIDTH-1 : 0]     awaddr,
	input wire [2 : 0] 		      awprot,
	input wire 			      awvalid,
	output wire 			      awready,

    //Write data channel
	input wire [AXI_DATA_WIDTH-1 : 0]     wdata,
	input wire [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
	input wire 			      wvalid,
	output wire 			      wready,
   
    //Write response channel
	output wire [1 : 0] 		      bresp,
	output wire 			      bvalid,
	input wire 			      bready,

    //Read address channel
	input wire [AXI_ADDR_WIDTH-1 : 0]     araddr,
	input wire [2 : 0] 		      arprot,
	input wire 			      arvalid,
	output wire 			      arready,

    //Read data channel
	output wire [AXI_DATA_WIDTH-1 : 0]    rdata,
	output wire [1 : 0] 		      rresp,
	output wire 			      rvalid,
	input wire 			      rready,
   
	input wire 			      aclk,
	input wire 			      aresetN
    );

   Caribou_control #(.AXI_DATA_WIDTH(AXI_DATA_WIDTH), .AXI_ADDR_WIDTH(AXI_ADDR_WIDTH))
   control(
	   //AXI4-Lite interface

	   //Write address channel
	   .awaddr(awaddr),
	   .awprot(awprot),
	   .awvalid(awvalid),
	   .awready(awready),

	   //Write data channel
	   .wdata(wdata),
	   .wstrb(wstrb),
	   .wvalid(wvalid),
	   .wready(wready),
      
	   //Write response channel
	   .bresp(bresp),
	   .bvalid(bvalid),
	   .bready(bready),

	   //Read address channel
	   .araddr(araddr),
	   .arprot(arprot),
	   .arvalid(arvalid),
	   .arready(arready),

	   //Read data channel
	   .rdata(rdata),
	   .rresp(rresp),
	   .rvalid(rvalid),
	   .rready(rready),
      
	   .aclk(aclk),
	   .aresetN(aresetN) );
   


endmodule // Caribou_control_wrapper

   
   
